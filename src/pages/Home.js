import PhotoBox from "../components/PhotoBox";
import Button from "../components/Button";
import { Link } from "react-router-dom";
import "./Home.css";

export default function Home() {
  return (
    <div className="home_page">
      <div className="center_block">
        <PhotoBox
          size="100"
          name="Giga Berdzenishvili"
          title="Front End engineer"
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque"
          avatar="https://scontent.ftbs2-2.fna.fbcdn.net/v/t1.6435-9/109117106_2671972889757641_4667134242806296810_n.jpg?_nc_cat=105&ccb=1-7&_nc_sid=be3454&_nc_ohc=kFCPqFfWPkkAX_1PheH&_nc_ht=scontent.ftbs2-2.fna&oh=00_AfCCcdH_gzY4JqXyEynV-zo2QaS5B4VqHDDWN7QsfcMMRg&oe=655B80D2"
        />
        <Link to="/about">
          <Button text="Learn more" />
        </Link>
      </div>
    </div>
  );
}
