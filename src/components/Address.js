import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { faPhone } from "@fortawesome/free-solid-svg-icons";
import {
  faFacebook,
  faTwitter,
  faSkype,
} from "@fortawesome/free-brands-svg-icons";

import "./styles/Address.css";

export default function Address() {
  return (
    <address className="contact_details">
      <ul>
        <li>
          <div className="icon">
            <FontAwesomeIcon icon={faPhone} />
          </div>
          <div>
            <strong>
              <a href="tel:500 342 242">500 342 242</a>
            </strong>
          </div>
        </li>

        <li>
          <div className="icon">
            <FontAwesomeIcon icon={faEnvelope} />
          </div>
          <div>
            <strong>
              <a href="mailto: office@kamsolutions.pl">
                office@kamsolutions.pl
              </a>
            </strong>
          </div>
        </li>

        <li>
          <div className="icon">
            <FontAwesomeIcon icon={faTwitter} />
          </div>
          <div>
            <strong>Twitter</strong>
            <br />
            <a href="https://twitter.com/wordpress">
              https://twitter.com/wordpress
            </a>
          </div>
        </li>

        <li>
          <div className="icon">
            <FontAwesomeIcon icon={faFacebook} />
          </div>
          <div>
            <strong>Facebook</strong>
            <br />
            <a href="https://www.facebook.com/facebook">
              https://www.facebook.com/facebook
            </a>
          </div>
        </li>

        <li>
          <div className="icon">
            <FontAwesomeIcon icon={faSkype} />
          </div>
          <div>
            <strong>Skype</strong>
            <br />
            <a href="skype:kamsolutions.pl?call">kamsolutions.pl</a>
          </div>
        </li>
      </ul>
    </address>
  );
}
