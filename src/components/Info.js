import "./styles/Info.css";
const Info = ({ text }) => {
  return (
    <div className="info">
      <p>{text}</p>
    </div>
  );
};

export default Info;
