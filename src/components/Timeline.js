import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchEducationData } from "../features/educationSlice";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSyncAlt } from "@fortawesome/free-solid-svg-icons";

import "./styles/Timeline.css";

export default function Timeline() {
  const dispatch = useDispatch();
  const { education } = useSelector((state) => state.education);
  const { status } = useSelector((state) => state.education);
  const { errorMessage } = useSelector((state) => state.education);

  useEffect(() => {
    dispatch(fetchEducationData());
  }, [dispatch]);

  if (status === "loading") {
    return (
      <div className="icon">
        <FontAwesomeIcon className="spinning_wheel" icon={faSyncAlt} />
      </div>
    );
  }

  if (status === "failed") {
    return (
      <div className="error">
        <p>{errorMessage}</p>
      </div>
    );
  }
  return (
    <ul className="timeline">
      {education.map((entry) => (
        <li key={`${entry.date}_${entry.title}`} data-testid="education_list">
          <div className="date">{entry.date}</div>
          <div className={`description info`}>
            <h3>{entry.title}</h3>
            <p>{entry.text}</p>
          </div>
        </li>
      ))}
    </ul>
  );
}
