import "./styles/PortfolioInfo.css";
export default function PortfolioInfo(props) {
  return (
    <article className="overlay_content">
      <h2 className="overlay_title">{props.title}</h2>
      <p className="overlay_text">{props.text}</p>
      <a href={props.source}>View source</a>
    </article>
  );
}
