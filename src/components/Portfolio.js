import { useState, useLayoutEffect } from "react";
import Isotope from "isotope-layout";
import "./styles/Portfolio.css";
import PortfolioInfo from "./PortfolioInfo";
import card1 from "../assets/card1.jpg";
import card3 from "../assets/card3.png";
export default function Portfolio() {
  const [isotope, setIsotope] = useState(null);
  const [filterKey, setFilterKey] = useState("*");

  useLayoutEffect(() => {
    setIsotope(
      new Isotope(`.filter_container`, {
        itemSelector: `.filter_item`,
        layoutMode: "fitRows",
      })
    );
  }, []);

  useLayoutEffect(() => {
    if (isotope) {
      filterKey === "*"
        ? isotope.arrange({ filter: `*` })
        : isotope.arrange({ filter: `.${filterKey}` });
    }
  }, [isotope, filterKey]);

  return (
    <>
      <ul className="tabs">
        <li onClick={() => setFilterKey("*")}>
          <span className={filterKey === "*" ? "active" : undefined}>All</span>
        </li>
        <li onClick={() => setFilterKey("ui")}>
          <span className={filterKey === "ui" ? "active" : undefined}>Ui</span>
        </li>
        <li onClick={() => setFilterKey("code")}>
          <span className={filterKey === "code" ? "active" : undefined}>
            Code
          </span>
        </li>
      </ul>
      <ul className="filter_container">
        <li className={`${"filter_item"} ui`}>
          <img height="190px" width="300px" src={card1} alt="card" />
          <div className="overlay">
            <PortfolioInfo
              title="Some title"
              text="Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis"
              source="www.somesite.com"
            />
          </div>
        </li>
        <li className={`${"filter_item"} code`}>
          <img height="190px" width="300px" src={card3} alt="card" />
          <div className="overlay">
            <PortfolioInfo
              title="Some title"
              text="Some text"
              source="www.somesite.com"
            />
          </div>
        </li>
        <li className={`${"filter_item"} code`}>
          <img height="190px" width="300px" src={card3} alt="card" />
          <div className="overlay">
            <PortfolioInfo
              title="Some title"
              text="Some text"
              source="www.somesite.com"
            />
          </div>
        </li>
        <li className={`${"filter_item"} ui`}>
          <img height="190px" width="300px" src={card3} alt="card" />
          <div className="overlay">
            <PortfolioInfo
              title="Some title"
              text="Some text"
              source="www.somesite.com"
            />
          </div>
        </li>
      </ul>
    </>
  );
}
