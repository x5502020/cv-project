import "./styles/PhotoBox.css";
export default function PhotoBox(props) {
  return (
    <div className={props.size === "small" ? "small" : "photoBox"}>
      <div className={"photo"}>
        <img
          width={props.size === "small" ? "100px" : "163px"}
          height={props.size === "small" ? "100px" : "163px"}
          src={props.avatar}
          alt="developer"
        />
      </div>
      <div>
        <h1 className={props.size === "small" ? "name_small" : "name"}>
          {props.name}
        </h1>
        <div>
          <p className="title">{props.title}</p>
          <p className="description">{props.description}</p>
        </div>
      </div>
    </div>
  );
}
