import "./styles/Button.css";

export default function Button(props) {
  const size = props.size;
  return (
    <>
      <button
        className={`${"button"} ${props.className} ${
          size === "mini" ? "mini" : ""
        }`}
        onClick={props.onClick}
        disabled={props.disabled}
      >
        <span>{props.icon}</span>
        <span className={"button_text"}>{props.text}</span>
      </button>
    </>
  );
}
