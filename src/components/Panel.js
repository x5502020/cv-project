import Navigation from "./Navigation";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import "./styles/Panel.css";
import React, { useState } from "react";
import Button from "./Button";
import PhotoBox from "./PhotoBox";

export default function Panel(props) {
  const [isVisible, setIsVisible] = useState(true);

  const togglePanelisVisible = () => {
    isVisible ? setIsVisible(false) : setIsVisible(true);
  };

  return (
    <>
      <div className="hamburger_menu" onClick={togglePanelisVisible}>
        {<FontAwesomeIcon icon={faBars} />}
      </div>
      <div className={isVisible ? "panel" : "panel hidden1"}>
        <div>
          {
            <PhotoBox
              size="small"
              name="Giga Berdzenishvili"
              avatar="https://avatars.githubusercontent.com/u/87497933?v=4"
            />
          }
        </div>
        <div>
          <Navigation />
        </div>
        <div className="empty"></div>
        <div>
          <Link to={props.backToRoute}>
            <Button
              icon={<FontAwesomeIcon icon={faChevronLeft} />}
              size="mini"
              text="Go back"
            />
          </Link>
        </div>
      </div>
    </>
  );
}
