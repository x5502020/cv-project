import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faGraduationCap,
  faUser,
  faPen,
  faGem,
  faSuitcase,
  faLocationArrow,
  faComment,
} from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-scroll";
import "./styles/Navigation.css";

const Navigation = () => {
  return (
    <nav>
      <ul className="nav">
        <li>
          <Link
            className="nav-link"
            to="About me"
            spy={true}
            smooth={true}
            duration={1000}
          >
            <div className="flex">
              <FontAwesomeIcon className="nav-icon" icon={faUser} />
              <p className="nav-section">About me</p>
            </div>
          </Link>
        </li>
        <li>
          <Link
            className="nav-link"
            to="Education"
            spy={true}
            smooth={true}
            duration={1000}
          >
            <div className="flex">
              <FontAwesomeIcon className="nav-icon" icon={faGraduationCap} />
              <p className="nav-section">Education</p>
            </div>
          </Link>
        </li>
        <li>
          <Link
            className="nav-link"
            to="Experience"
            spy={true}
            smooth={true}
            duration={1000}
          >
            <div className="flex">
              <FontAwesomeIcon className="nav-icon" icon={faPen} />
              <p className="nav-section">Experience</p>
            </div>
          </Link>
        </li>
        <li>
          <Link
            className="nav-link"
            to="Skills"
            spy={true}
            smooth={true}
            duration={1000}
          >
            <div className="flex">
              <FontAwesomeIcon className="nav-icon" icon={faGem} />
              <p className="nav-section">Skills</p>
            </div>
          </Link>
        </li>
        <li>
          <Link
            className="nav-link"
            to="Portfolio"
            spy={true}
            smooth={true}
            duration={1000}
          >
            <div className="flex">
              <FontAwesomeIcon className="nav-icon" icon={faSuitcase} />
              <p className="nav-section">Portfolio</p>
            </div>
          </Link>
        </li>
        <li>
          <Link
            className="nav-link"
            to="Contacts"
            spy={true}
            smooth={true}
            duration={1000}
          >
            <div className="flex">
              <FontAwesomeIcon className="nav-icon" icon={faLocationArrow} />
              <p className="nav-section">Contacts</p>
            </div>
          </Link>
        </li>
        <li>
          <Link
            className="nav-link"
            to="Feedback"
            spy={true}
            smooth={true}
            duration={1000}
          >
            <div className="flex">
              <FontAwesomeIcon className="nav-icon" icon={faComment} />
              <p className="nav-section">Feedback</p>
            </div>
          </Link>
        </li>
      </ul>
    </nav>
  );
};

export default Navigation;
