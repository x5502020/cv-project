import "./styles/Box.css";

export default function Box({ title, content }) {
  return (
    <section className="info_box">
      <h2 id={`${title}`}>{title}</h2>
      <div>{content}</div>
    </section>
  );
}
