import React from "react";
import avatar from "../assets/avatar1.png";
import "./styles/Feedback.css";
const Feedback = ({ data }) => {
  return (
    <div className="feedback-list">
      <div className="feedback-items">
        {data.map((item, index) => (
          <div className="feedback-item" key={index}>
            <div className="feedback-text">{item.feedback}</div>
            <div className="reporter">
              <img className="photo1" src={avatar} alt={item.reporter.name} />
              <div>
                <a
                  href={item.reporter.citeUrl}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {item.reporter.name}
                </a>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Feedback;
