import { educationReducer } from "../features/educationSlice";
import { skillReducer, skillsMiddleware } from "../features/skillsSlice";
import thunk from "redux-thunk";
import { configureStore } from "@reduxjs/toolkit";

export const store = configureStore({
  reducer: {
    education: educationReducer,
    skills: skillReducer,
  },
  middleware: [skillsMiddleware, thunk],
});

store.dispatch({ type: "APP_INIT" });
